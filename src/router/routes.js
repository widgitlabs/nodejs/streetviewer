const routes = [
  {
    path: "/",
    component: () => import("src/layouts/MainLayout.vue"),
    children: [
      { path: "/", component: () => import("pages/HomePage.vue") },
      { path: "import", component: () => import("pages/ImportPage.vue") },
      { path: "history", component: () => import("pages/HistoryPage.vue") },
      { path: "settings", component: () => import("pages/SettingsPage.vue") },
      { path: "help", component: () => import("pages/HelpPage.vue") },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
