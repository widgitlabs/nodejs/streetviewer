# StreetViewer

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/nodejs/streetviewer/blob/main/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/nodejs/streetviewer/badges/main/pipeline.svg)](https://gitlab.com/nodejs/streetviewer/cartography/pipelines)

Grab a Street View snapshot for a given address. Requires a Google Cloud project with billing enabled.

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev -m electron '--' --no-sandbox --disable-setuid-sandbox
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build -m electron
```
